//console.log("Hello World!");

let firstName = "John";
let lastName = "Smith";
let yourAge = 30;

console.log("First Name: " + firstName)
console.log("Last Name: " + lastName)
console.log("Age: " + yourAge)

let hobby1 = "Biking";
let hobby2 = "Mountain Climbing";
let hobby3 = "Swimming";

console.log("Hobbies:");
console.log(`(3) ${hobby1} ${hobby2} ${hobby3}`)

console.log("Work Address:");

let houseNumber = 32;
let street = "Washington";
let city = "Lincoln";
let state = "Nebraska";

console.log('{houseNumber: "' + houseNumber + '", street: "' + street + '", city: "' + city + '", state: "' + state + '"}');

function printUserInfo (firstName, lastName, age){
	console.log(firstName + " " + lastName + " is " + age + " years of age.")
};
printUserInfo("John", "Smith", 30);

function printMe(){
	console.log("This was printed inside of a function")
};
printMe()

function myHobbies (hobby1, hobby2, hobby3){
	console.log("(3)[" + hobby1 + ", " + hobby2 + ", " + hobby3 + "]")
};
myHobbies("Biking", "Mountain Climbing", "Swimming");

printMe()

console.log('{houseNumber: "' + houseNumber + '", street: "' + street + '", city: "' + city + '", state: "' + state + '"}');

function isMarried(){
	console.log("isMarried: true")
};
isMarried()