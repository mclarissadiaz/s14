//JS Functions
/*
functions are used to create reusable commands/statements that prevents dev from typing a bunch of codes. in field, a big # of codes is normal output; using functions would save devs a lot of time & effort in typing codes to b eused multiple times.

SYNTAX
function functionName(parameters){
	command/statement
};

*/
function printStar(){
	console.log("*")
};

printStar();
printStar();
printStar();
printStar();

/*
functions can also use paramenters; parameters can be defined and part of command inside the function.
	when called, paramenters can be replaced w a target value of developer. make sure that value is inside quoration when called (if string)
*/
function sayHello(name){
	console.log("Hello " + name)
};

sayHello("Cris");


/*function alertPrint(){
	alert("Hello");
	console.log("Hello")
};
alertPrint();*/

//function that accepts 2 numbers and prints sum.

function add(x,y){
	let sum = x + y;
	console.log(sum)
};

add(1,2);
//add(1,2,3); if numebr exceeds parameters defined, excess will be ignored by JS

//3 Parameters

function printBio(fname, lname, age){
	//console.log("Hello " + fname + " " + lname + " " + age)

	//using template literals
//backticks - symbol to the left of 1 (``)
/*
	declared by using backticks w/ $ and {} with parameters inside braces. displayed data in console will be the defined parameter insted of text in curly braces.
*/

	console.log(`Hello ${fname} ${lname} ${age}`)
}; 

printBio("Cris", "Cristobal", 21);

function createFullName (fname, mname, lname){
	//return specifies value to be given back by function once it is finished executing. the value can be given to a variable. thats why we also need to log variable in console outside the function statement. (use return if there's a long list to be logged; for shorter list, can use console.log directly)
	return `${fname} ${mname} ${lname}`
};

let fullName = createFullName("Juan", "Dela", "Cruz");
console.log(fullName);