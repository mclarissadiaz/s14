//one line comment (ctrl+slash)
/*
multiline comment (ctrl+shift+slash)
*/

console.log("Hello Batch 170!");

/*
javascript - we can see the messages/log in the console.

browser consoles - part of browser that allows us to see/log messages/data/info from programming language.
	easily accessed in dev tools

console.log("Hello Batch 170!"); >>> statement

statements - instructions/expressions we add to programming language to communicate with computers.
	usually ends with semicolon (;) but JS has implemented a way to automatically add semicolon at end of line.

syntax - set of rules describing how statements should be made/constructed	
	lines/blocks of codes must follow a certain set of rules for it to work properly
*/

console.log("Cris Cristobal");

/* mini activity: 3console logs of fave food*/

console.log("Udon");
console.log("Pizza");
console.log("Sushi");

//another way of making the statement:
let food1="Udon";

console.log(food1);

/*
variables - storage of info/data within JS (LIKE A CONTAINER OF VALUE)
	to create variable, declare name of variable using let/const keyword:
	
	let <nameOfVariable>

	then we can initialize/define variable using the value/data

	let <nameOfVariable>=<valueOfVariable>
*/

console.log("My favorie food is "+food1);

let food2="Pizza";

console.log("My favorite food is:"+food1+" and "+food2);

let food3;


/*
we can create variables w/o values, but variables' content would log undefined.
*/

console.log(food3);

food3="Sushi";
/*
we can update content of variable by reassigning value using an assignment operator (=);

assignment operator (=) lets us (re)assign values to a variable.
*/
console.log(food3);

/* cannot use a variable for 2 different values;
let food1 = "flat tops";
console.log(food1);
*/

// const keyword
/*
	const - will allow creation of variable; but with const keyword, we create constant variable. Meaning the data saved in these variables will not change, should not & cannot be changed.
*/
const pi = 3.1416;
console.log(pi);
/*pi = "Pizza";
console.log(pi);
*/
/* we cant create const variable w/o initialization or assigning its value
const gravity;
console.log(gravity);
*/

/*mini activity
let keyword
	reassign new value for food1 with another favorite food
	same for food2
	log values of both variables in console 

const keyword
	create const called sunriseDirection w/ east as value
	const called sunsetDirectoin w/ West as value
	log both values of variable in console	
*/


food1 = "quesadilla";
console.log(food1);

food2 = "fruits";
console.log(food2);

const sunriseDirection = "East";
console.log(sunriseDirection);

const sunsetDirection = "West";
console.log(sunsetDirection);

/*
guidelines in creating js variable

	1. can create let variable w/ let keyword; can be reassigned but not redeclared (use let only once)
	2. creating variable has 2 parts: declare variable name; & initialization of initial value of variable through use of assignment operator(=)
	3. let variable can be declared w/o initialization(no assignment of values). but the value of variable will be undefined until re-assigned w/ a value
	4. not defined vs undefined - not defined error means variable is used but not declared. undefined results from variable thats used but not initialized(no value assigned).
	5. can use const keyword to create variables. constant variables cant be declared w/o initialization (assign value). Const values cannot be reassigned.
	6. when creating variable names, start w/ small caps, no space in between words & capitalize for second word; this is to avoid conflict w/ JS thats named w/ capital letters
	7. if variable needs 2 words, naming convention is use camelCase. no space in between words for the variables.
*/
//Data Types

/*
string
	are alphanumeric data types. Could be name, phrase or sentence. We can create them with single quote(') or double quote("). The text inside quote will be displayed as it is.

	keyword variable = "string data" 
*/
console.log("Sample String Data");

/*
numeric data types
	data types which are numeric; numeric data types are displayed when numbers are not placed in quotation marks.
	if there is mathematical operation to be performed, use numeric data type instead of string.

	keyword variable = numeric data;
*/
console.log(0123456789);
console.log(1+1);
console.log("1+1");

//MiniActivity

console.log("Cris Cristobal")
console.log("Sep6")
console.log("Cavite")

console.log(100)
console.log(8)
console.log(3)